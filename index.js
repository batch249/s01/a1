// S01-A1
// QUIZ ANSWER
/*
1.) let array_name = [ item1, item2 ];
2.) array_name[0];
3.) array_name[array_name.length-1];
4.) array_name.indexOf("search_value");
5.) forEach()
6.) map()
7.) every()
8.) some()
9.) False
10.) True

*/

// FUNCTION CODING
// 1 -----------------------------

let students= ["John", "Joe", "Jane", "Jessie"];

let addToEnd = (array, string) => {

	if (typeof string == "string") {
		array.push(string);
		return array;
	}else {
		return `error - can only add strings to an array`;
	}

}

console.log(addToEnd(students, "Ryan"));
console.log(addToEnd(students, 045));

// 2 -----------------------------
let addToStart = (array, string) => {

	if (typeof string == "string") {
		array.unshift(string);
		return array;
	}else {
		return `error - can only add strings to an array`;
	}
}

console.log(addToStart(students, "Tess"));
console.log(addToStart(students, 033));


// 3 -----------------------------
let elementChecker = (array, newValue) => {

	if (array.length == 0) {
		return `error -passed in array is empty`;
	}else {
		return array.some(checkIfExisting = (name) => name == newValue );
	}

}

console.log(elementChecker(students, "Jane"));
console.log(elementChecker([], "Jane"));



// 4 -----------------------------
let checkAllStringsEnding = (array, char) => {
	if (array.length == 0) {
		return `error - array must NOT be empty`;
	}else if (array.some(checkIfNotString = (value) => typeof value != "string" ) == true) {
		return `error - all array elements must be strings`;
	}else if (typeof char != "string"){
		return `error - 2nd argument must be a of data type string`;
	}else if ( char.length > 1){
		return `error - 2nd argument must be a single character`;
	}else {
		return array.every(checkIfEndingInChar = (value) => value[value.length-1] == char);
	}
}

console.log(checkAllStringsEnding(students, "e"));
console.log(checkAllStringsEnding([], "e"));
console.log(checkAllStringsEnding(["Jane", 02], "e"));
console.log(checkAllStringsEnding(students, "el"));
console.log(checkAllStringsEnding(students, 4));

// 5 -----------------------------
let stringLengthSorter = (array) => {
	if (array.some(checkIfNotString = (value) => typeof value != "string") == true) {
		return `error - all array elements must be strings`;
	}else {
		array.sort((a, b) => a.length - b.length);
		return array;
	}
}

console.log(stringLengthSorter(students));
console.log(stringLengthSorter([037, "John", 039, "Jane"]));

// 6 -----------------------------
let startsWithCounter = (array, char) => {
	if (array.length == 0) {
		return `error - array must NOT be empty`;
	}else if (array.some(checkIfNotString = (value) => typeof value != "string" ) == true) {
		return `error - all array elements must be strings`;
	}else if (typeof char != "string"){
		return `error - 2nd argument must be a of data type string`;
	}else if ( char.length > 1){
		return `error - 2nd argument must be a single character`;
	}else {
		let count=0;
		array.forEach(value =>{
			if (value.charAt(0) == char) {
				count++;
			}
		})
		return count;
	}
}

console.log(startsWithCounter(students, "J"));